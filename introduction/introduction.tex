% !TEX encoding = UTF-8 Unicode
\providecommand{\main}{..} %Important: It needs to be defined before the documentclass
\documentclass[english,\main/main/lhec_2019.tex]{subfiles} % document type and language
% graphics path(s) for this section
\graphicspath{{\main/main/figures/}{\main/main/}{\main/introduction/figures/}{\main/introduction/}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do not edit!
\begin{document}
\linenumbers
\lhectitlepage
\lhecinstructions
\subfilestableofcontents


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\chapter{Introduction}\label{sec:intro}
%
\section{The Context}
\subsection{Particle Physics - at the Frontier of Fundamental Science}
% 
%
Despite its striking success, the Standard Model (SM) has been recognised 
to have major deficiencies. These may be summarised in various ways. 
Some major questions can be condensed as follows:
%
\begin{itemize}
\item $\textbf{Higgs~boson}$~~Is the electroweak scale stabilised by new particles, interactions, symmetries? Is the Higgs boson discovered in 2012 the SM Higgs boson, what is its potential? Do more Higgs bosons exist as predicted, for example, in super-symmetric theories?
\item $\textbf{Elementary~Particles}$~~The SM has 61 identified particles: 12 leptons, 36 quarks and anti-quarks, 12 mediators, 1 Higgs boson. Are these too many or too few? Do right-handed neutrinos exist? Why are there three families? What makes leptons and quarks different? Do leptoquarks exist, is there a deeper substructure?
\item $\textbf{Strong~Interactions}$~~What is the true parton dynamics and structure inside the proton, inside other hadrons
 and inside nuclei -- at different levels of resolution? How is confinement explained and how do partons hadronise? How can the many body dynamics of the Quark Gluon Plasma (QGP) state be described in terms of the elementary fields of Quantum Chromodynamics? What is the meaning of the AdS/CFT relation and of supersymmetry in strong interactions? Do axions, odderons, instantons exist?
\item $\textbf{GUT}$~~Is there a genuine, grand unification of the interactions at high scales, would this include gravitation? What is the correct value of the strong coupling constant, is lattice theory correct in this respect? Is the proton stable?
\item $\textbf{Neutrinos}$~~Do Majorana or/and sterile neutrinos exist, is there CP violation in the neutrino sector?
\item $\textbf{Dark Matter}$ Is dark matter constituted of elementary particles or has it another origin? Do 
hidden or dark sectors of nature exist and would they be accessible to accelerator experiments?
\end{itemize}
%
%
These and other open problems are known, and they have been persistent questions to Particle Physics. They are intimately related and any future strategic programme should not be confined to only one or a few of these. The field of particle physics is far from being understood, despite the phenomenological success of the
SU$_L$(2)\,$\times$\,U(1)\,$\times$\,SU$_c$(3) gauge field theory termed the Standard Model.  Certain attempts to declare its end %end~\cite{Hossenfelder:2018jew} 
are in contradiction not only to the
 experience from a series of past revolutions in science but indeed contrary to the
 incomplete status of particle physics as sketched above.  
%despite their perhaps stimulating irritation, may possibly be compared with Kelvin's prediction about the 
%future which comprised precision only as all principal questions had already been solved, back in
%around 1900. 
The question is not why to end particle physics but how to proceed. 
The answer is not hidden in philosophy but requires new, better, affordable experiments.
Indeed the situation is special as expressed by Guido Altarelli a few years ago: {\emph{It is now less 
unconceivable that no new physics will show up at the 
LHC$\dots$ We expected complexity and instead we have found a maximum of simplicity.
The possibility that the Standard model holds well beyond the electroweak 
scale must now be seriously considered~\cite{Altarelli:2014xxa}}}. This is reminiscent
of the time before 1969, prior to anything like a Standard Model, when gauge theory
was just for theorists, while a series of new accelerators, such as the 2\,mile electron linac
at Stanford or the SPS at CERN, were planned which resulted in a complete change of the 
paradigm of particle physics. 
%, dedicated
%ones and general purpose $hh$, $ee$ and $eh$ colliders of complementary kind, comparable to
%the TeVatron, LEP/SLC and HERA a few decades ago.

%Major challenges for particle physics also comprise basic puzzles 
%on the Universe, especially on the nature of Dark Matter. 
Ingenious  theoretical hypotheses, such as on the existence of extra dimensions, 
on SUSY, of un-particles or the embedding in higher gauge groups, 
like E8, are a strong motivation to develop high energy physics 
rigorously further. In this endeavour, a substantial increase of 
precision, the conservation of diversity of projects and the extension of kinematic 
coverage are  a necessity, likely turning out to be of fundamental importance. 
The strategic question in this context, therefore, is not just which new collider
should be built next, as one often hears, but how we may challenge the current and incomplete
knowledge best. A realistic step to progress comprises a new $e^+e^-$ collider, built
perhaps in Asia, and  complementing the LHC  with an electron energy recovery linac
to synchronously operate $ep$ with $pp$ at the LHC, the topic of this paper.  

One may call these machines first technology generation colliders as their technology 
has been proven to principally work~\cite{jdhecfa}.  Beyond these times,
there is a long-term future reaching to the year 2050 and much beyond, 
of a second, further generation of hadron, lepton and electron-hadron colliders.
%which is not considered in the present paper.
%~\footnote{There are a few
%exceptions when studies are presented not only for the LHeC but also the $ep$ version
%of the Future Circular Collider, the FCC-eh, which uses, seen from today, the same ERL 
%technology and possibly hardware as is currently under development for the LHeC.}.
CERN has recently published a design study 
 of a future circular $hh,~eh$ and $e^+e^-$ collider (FCC)  
 complex~\cite{Abada:2019lih, Abada:2019zxq, Benedikt:2018csr},
  which would provide a corresponding base. For electron-hadron scattering this opens a new  horizon with the FCC-eh, an about $3$\,TeV centre-of-mass system (cms) energy collider which in this
  paper is also considered, mostly for comparison with the LHeC.
  A prospect similar to FCC is also being developed 
  in China~\cite{CEPC-SPPCStudyGroup:2015esa,CEPCStudyGroup:2018ghi}.

A new collider for CERN at the level of $\mathcal{O}(10^{10})$ CHF cost should have
the potential to change the paradigm of particle physics with 
direct, high energy discoveries in the $10$\,TeV mass range. This may
only be achieved with the FCC-hh including an $eh$ experiment. 
The FCC-hh/eh complex does access physics to several 
hundred TeV,  assisted by a qualitatively new level of QCD/DIS. 
A prime, very fundamental goal of the 
FCC-pp is the clarification of the Higgs vacuum potential which can not be
achieved in $e^+e^-$. This collider therefore has an overriding justification
beyond the unknown prospects of finding new physics nowadays termed ``exotics". 
It accesses rare Higgs boson decays, high scales and, when combined 
with $ep$, it measures the SM Higgs couplings to below percent precision. 
There is a huge, fundamental program on electroweak and strong 
interactions, flavour and heavy ions for FCC-hh to be explored. 
This represents CERN’s unique opportunity 
to build on the ongoing LHC program, for many decades ahead. 
%The FCC-hh
%requires high magnetic field dipoles. 
%A strongly supported magnet R$\&$D program 
%shall find an affordable, high field solution, to be selected in the early
%thirties, and not now. 
The size of the FCC-hh requires
this to be established as a global enterprise. The HL-LHC and the LHeC can be understood as 
very important steps towards this major new facility, both in terms of physics and technology. 
The present report outlines a road towards realising a next generation, energy frontier
electron-hadron collider as part of this program, which would maximally exploit and support
the LHC.


%
\subsection{Deep Inelastic Scattering and HERA}
%
The field of deep inelastic lepton-hadron
 scattering (DIS)~\cite{Feynman:1973xc}  was born
with the discovery~\cite{Bloom:1969kc,Breidenbach:1969kd} 
of partons~\cite{Feynman:1969ej,Bjorken:1969ja} about $50$ years ago.
It readily contributed fundamental insights, for example on the development of QCD with the confirmation of fractional quark charges and of asymptotic freedom or 
with the spectacular  finding that the  weak isospin
charge of the right-handed electron was zero~\cite{Prescott:1978tm} which established the
Glashow-Weinberg-Salam ``Model of Leptons"~\cite{Weinberg:1967tq}  as the 
base of the united electroweak theory.  The quest to reach higher energies in accelerator based particle physics
led to generations of colliders, with HERA~\cite{Wiik:1985sb} as the so far only electron-proton one.

HERA collided electrons (and positrons) of $E_e=27.6$\,GeV energy off protons
of $E_p=920$\,GeV energy achieving a centre-of-mass energy, $\sqrt{s}=2 \sqrt{E_e E_p}$,
of about $0.3$\,TeV. It therefore
extended the kinematic range covered by fixed target experiments by two 
orders of magnitude in  Bjorken $x$ and in four-momentum transfer squared, 
$Q^2$, with its limit $Q^2_{max}=s$.
 HERA was built in less than a decade, and it operated for 16 years. 
Together with the Tevatron and LEP, HERA was pivotal to the development of the Standard Model.

HERA had a unique collider physics programme and success~\cite{Klein:2008di}.
It established QCD  as the correct description of proton
substructure and parton dynamics down to $10^{-19}$\,m. It demonstrated electroweak
theory to hold in the newly accessed range, especially with the measurement of neutral
and charged current $ep$ scattering cross sections beyond  $Q^2 \sim M_{W,Z}^2$ and
with the proof of electroweak interference at high scales through the measurement of 
the interference structure functions $F_2^{\gamma Z}$ and  $xF_3^{\gamma Z}$. The HERA
collider has provided the core base of the physics of parton distributions, not only in determining the
gluon, valence, light and heavy sea quark momentum 
distributions in a much extended range,
 but as well in  supporting the foundation of the theory of unintegrated, 
diffractive, photon, neutron PDFs through a series of corresponding measurements.
It discovered the rise of the parton distributions towards small momentum fractions, $x$,
supporting early QCD expectations on the asymptotic behaviour of the structure
functions~\cite{DeRujula:1974mnv}.   
Like the TeVatron and LEP/SLC colliders which  explored the Fermi scale of a few hundred GeV energy,
determined by the vacuum expectation value of the Higgs field, $v = 1/ \sqrt{\sqrt{2} G_F} =
2 M_W/g \simeq 246$\,GeV, HERA showed too that there was no supersymmetric
or other exotic particle with reasonable couplings existing at the Fermi energy scale.

HERA established electron-proton scattering as an integral part of modern high energy
particle physics. It demonstrated the 
richness of DIS physics, and the feasibility of constructing and operating energy frontier $ep$ colliders.
% It is a testimony of the vision and authority of Bjoern Wiik.
 What did we learn to take into
a next, higher energy $ep$ collider design? Perhaps there  arose three lessons about: 
\begin{itemize}
\item \emph{the~need~for~higher~energy}, for three reasons: i) 
to make  charged currents a real, precision part of $ep$ physics, for instance for the complete unfolding
of the flavour composition of the sea and valence quarks, ii) to produce heavier mass particles (Higgs, top,
exotics) with favourable cross sections, and iii) to discover or disproof the existence of 
gluon saturation for which one needs to measure at lower $x \propto Q^2/s$, i.e. higher
$s$ than HERA had available;
\item
\emph{the~need~for~much~higher~luminosity}: the first almost ten years of HERA provided 
just a hundred pb$^{-1}$. As a consequence, HERA could not accurately
 access the high $x$ region, and it was inefficient and short of statistics in resolving 
 puzzling event fluctuations; 
 \item  \emph{the~complexity~of~the~interaction~region} when
 a bent electron beam caused synchrotron radiation while the opposite proton beam
 generated quite some halo background through beam-gas and beam-wall proton-ion interactions.
% This we had not seen clearly enough prior to and during the initial phase of the 
% HERA luminosity upgrade.
 \end{itemize}
Based on these and further lessons a first LHeC paper was published in 
2006~\cite{Dainton:2006wd}. The LHeC design was then intensely worked on,
and a comprehensive CDR appeared in 2012~\cite{AbelleiraFernandez:2012cc}. 
This has now been pursued much further still recognising that 
the LHC is the only existing base to realise a TeV energy scale electron-hadron collider in the
accessible future. It offers highly energetic, intense hadron beams, a long time perspective
and a unique infrastructure and expertise, i.e. everything required for an energy frontier
DIS physics and innovative accelerator programme.
%
\section{The Paper}
\subsection{The LHeC Physics Programme}
%
This paper presents a design concept of the LHeC, using a $50$\,GeV energy
electron beam to be scattered off the LHC hadron beams (proton and ion) in concurrent
operation\footnote{The CDR in 2012 used a 60\,GeV beam energy. Recent considerations of
cost, effort and synchrotron radiation effects led to preference of a small reduction of the
energy. Various physics studies presented here still use $60$\,GeV. While for BSM, top and Higgs physics the high energy is indeed important, the basic conclusions remain valid if eventually the energy was indeed
chosen somewhat smaller than previously considered. This is further discussed below.
A decision on the energy would come with the approval obviously.}.
Its main characteristics are presented in \textbf{Chapter\,2}.
The instantaneous luminosity is designed to be $10^{34}$\,cm$^{-2}$s$^{-1}$ 
exceeding that of HERA, which
achieved a few times $10^{31}$\,cm$^{-2}$s$^{-1}$, by a factor of several hundreds. The kinematic
range nominally is extended by a factor of about $15$, but in fact by a larger amount because 
of the hugely increased luminosity which is available for exploring the maximum $Q^2$ and 
large $x  \leq 1$ regions, which were major deficiencies at HERA. 
The coverage of the $Q^2,~x$ plane
by previous and future DIS experiments is illustrated in Fig.\,\ref{fig:kinplane}.
%
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth,trim={0 70 0 70},clip]{kinecdr.pdf}
\caption{
Coverage of the kinematic plane in deep inelastic lepton-proton scattering 
by some initial fixed target experiments, with electrons (SLAC) and
muons (NMS, BCDMS), and by the $ep$ colliders:  the EIC (green), HERA (yellow), the LHeC (blue) and 
the FCC-eh (brown). The low $Q^2$ region for the colliders is here limited to about $0.2$\,GeV$^2$, 
which is covered by the central detectors, roughly and perhaps using low electron
beam data. Electron taggers may extend this to even lower $Q^2$.
The high $Q^2$ limit at fixed $x$ is given by the line of inelasticity $y=1$.  Approximate limitations
of acceptance at medium $x$, low $Q^2$ are illustrated using polar angle limits of 
$\eta = - \ln \tan \theta /2$ of $4,~5,~6$ for the EIC, LHeC, and FCC-eh, respectively.
These lines are given by $x= \exp {\eta} \cdot \sqrt{Q^2}/(2 E_p)$, and can be moved to larger $x$
when $E_p$ is lowered below the nominal values.}.
\label{fig:kinplane}
\end{figure}
%

The LHeC would provide a major extension of the DIS kinematic range as is
required for the physics programme at the energy frontier. 
For the LHC, the $ep$/A detector would be a new major experiment.
  A number of major themes would be 
 explored with significant discovery potential. These are presented in quite some detail in 
 seven chapters of this paper dedicated to physics:    
%
\begin{itemize}
\item
Based on the unique hadron beams of the LHC and employing a point-like
probe, the LHeC would represent the world's cleanest, high resolution
microscope for exploring the substructure of and dynamics inside matter,
which  may be termed the Hubble telescope for the smallest dimensions. 
The first chapter on physics, \textbf{Chapter\,3}, is devoted to the measurement 
of parton distributions with the LHeC, and it also presents the potential to resolve
proton structure in 3D. 

\item 
\textbf{Chapter\,4} is devoted to the deep exploration of QCD. A key deliverable of the
LHeC is the clarification of the parton interaction dynamics at small Bjorken $x$,
in the new regime of very high parton densities but small coupling which HERA 
discovered but was unable to clarify for its energy was limited. It is first shown
that the LHeC can measure  $\alpha_s$ to per mille accuracy followed by 
various studies to illustrate the unique potential of the LHeC to pin down the 
dynamics at small $x$. The chapter also covers the seminal potential for diffractive DIS 
to be developed. It concludes with brief presentations on theoretical developments
on pQCD and of novel physics on the light cone.

\item
The maximum $Q^2$ exceeds the $Z,~W$ boson mass values (squared) by two orders of magnitude. The LHeC, supported by
variations of beam parameters and high luminosity, thus offers a unique potential to test the electroweak SM in the
spacelike region with unprecedented precision.
The high $ep$ cms energy leads to the copious production of
top quarks, of about $2 \cdot 10^6$ single top and $  5 \cdot 10^4$ $t \bar{t}$ events. Top
production could not be observed at HERA but will thus become a
central theme of precision and discovery physics with the LHeC.
In particular, the top momentum fraction, 
top couplings to the photon, the $W$ boson and possible flavour changing neutral currents
(FCNC) interactions can be studied in a uniquely clean environment (\textbf{Chapter\,5}).

\item
The LHeC 
%leads into the region of high parton densities at small Bjorken $x$
%in protons and in nuclei.
 extends the kinematic range in lepton-nucleus scattering 
by nearly four orders of magnitude. It thus will transform 
nuclear particle physics completely, by resolving the hitherto hidden parton dynamics and substructure in nuclei and clarifying the QCD base for the collective dynamics 
observed in QGP phenomena (\textbf{Chapter\,6}).
\item
The clean DIS final state in neutral and charged current scattering  and the 
high integrated luminosity  enable a
high precision Higgs physics programme with the LHeC. The Higgs production
cross section is comparable to the one of Higgs-strahlung at $e^+e^-$. This opens
unexpected extra potential to independently test the Higgs sector of the SM,
with high precision 
insight especially into the $H - WW/ZZ$ and $H - bb/cc$ couplings (\textbf{Chapter\,7}).
% if it reaches 
%O($10^{34}$) cm$^{-2}$s$^{-1}$ luminosity.
\item
As  a new, unique, luminous TeV scale
collider, the LHeC has an outstanding opportunity to discover new
physics, such as in the exotic Higgs, dark matter, heavy neutrino and QCD
areas (\textbf{Chapter\,8}).
\item
With concurrent $ep$ and $pp$ operation, the LHeC would transform the
LHC into a 3-beam, twin collider of greatly improved potential 
which is sketched in \textbf{Chapter\,9}. 
Through ultra-precise strong and electroweak measurements,
the $ep$ experiment would make the HL-LHC complex  a much more powerful
search and measurement laboratory than current expectations, based on $pp$
only, do entail.
 The joint $pp/ep$ LHC facility together with a 
novel $e^+e^-$ collider will make a major step in the study of the 
SM Higgs Boson, leading far beyond the HL-LHC. Putting $pp$ and $ep$
results together, as is illustrated for PDFs, will lead to new insight, especially
when compared with its single $pp$ and $ep$ components.

%
\end{itemize}
%
The development of particle physics, 
the future of CERN, the exploitation of the singular LHC investments,
 the culture of accelerator art, 
 all make the  LHeC a unique project of great interest. It is challenging in terms of technology,
 affordable given  budget constraints and it may still be realised in the two decades of currently
 projected LHC lifetime. 


\subsection{The Accelerator}
%
The LHeC  provides an intense, high energy electron beam 
to collide with the LHC.  It represents the highest energy application of
energy recovery linac (ERL) technology which is increasingly recognised as one of the
major pilot technologies for the development of particle physics because
 it utilises and stimulates superconducting RF technology progress, and it
 increases intensity while keeping the power consumption low.
 
The LHeC instantaneous luminosity is determined through the integrated luminosity 
goal of $\mathcal{O}(1)$\,ab$^{-1}$ caused by various physics reasons.
%, not least the
%Higgs $ep$ programme. 
The electron beam energy is
chosen to achieve  TeV cms collision energy  and enable competitive searches 
and precision Higgs boson measurements. A cost-physics-energy evaluation
is presented here which points to choosing $E_e \simeq 50$\,GeV as a new 
default value, which was $60$\,GeV before~\cite{AbelleiraFernandez:2012cc}.
The wall-plug power has been constrained to $100$\,MW. 
Two super-conducting linacs of about $900$\,m length, which are placed opposite to each other,
accelerate the passing electrons by $8.3$\,GeV each. This
leads to a final electron beam energy of about $50$\,GeV in 
a 3-turn racetrack energy recovery linac  configuration.
%as   is illustrated in Fig.\,\ref{figlhec}. 
 
 For measuring at very low $Q^2$ 
and for determining the longitudinal 
structure function $F_L$, see below, the electron beam energy may be reduced to 
a minimum of  about $10$\,GeV. For maximising the acceptance at large Bjorken
$x$, the proton beam energy, $E_p$, may be reduced to $1$\,TeV. 
This determines a minimum cms energy of $200$\,GeV, below HERA's $319$\,GeV.
%, i.e.
%the LHeC will be able to completely redo the HERA collider programme should that 
%be of interest.
 If the ERL may be combined in the further future with the double energy
 HE-LHC~\cite{Abada:2019ono}, the proton beam energy $E_p$
could reach $14$\,TeV and $\sqrt{s}$ be increased to $1.7$\,TeV.
This is extended to $3.5$\,TeV for the FCC-eh with a $50$\,TeV proton energy beam.
We thus have the unique, exciting prospect for  future DIS $ep$ scattering at CERN with an
energy range from below HERA to the few TeV region, at hugely increased luminosity
and based on much more sophisticated experimental techniques than had been available
at HERA times.

A spectacular extension of the
kinematic range will be expected for deep inelastic lepton-nucleus scattering which 
was not pursued at DESY. Currently,
highest energy $lN$ data are due to fixed target muon-nucleus experiments, such as 
NMC and COMPASS, with a maximum $\sqrt{s}$ of about $20$\,GeV which permits a maximum $Q^2$
of $400$\,GeV$^2$. This will be extended with the EIC at Brookhaven
to about $10^4$\,GeV$^2$.
 The corresponding numbers for $e$Pb scattering at LHeC (FCC-eh) are
$\sqrt{s} \simeq 0.74~(2.2)$\,TeV and $Q^2_{max} = 0.54~(4.6)~10^6$\,GeV$^2$. 
The kinematic range in $e$A scattering  will thus be extended through the LHeC (FCC-eh)  
by three (four) orders of magnitude as compared to the current status. This
will thoroughly alter the understanding of parton and collective dynamics inside nuclei.


The ERL beam configuration
%, Fig.\,\ref{figlhec}, 
is located inside the LHC ring but
outside its tunnel, which minimises any interference
with the main hadron beam infrastructure. The electron accelerator may thus
be built independently, to a considerable extent, of the status of operation
of the proton machine.  
%This is the principal advantage of the Linac-Ring
%over the Ring-Ring configuration which had been studied to considerable 
%detail in the CDR also.
The length of the  ERL has configuration to be a fraction
$1/n$ of the LHC circumference as is required 
for the $e$ and $p$ matching of bunch patterns. Here the
return arcs count as two single half rings. 
The chosen electron beam energy  of $50$\,GeV
leads, for $n=5$, to a circumference $U$ of $5.4$\,km for the electron
racetrack~\footnote{The circumference
may eventually be chosen to be $6.8$\,km, the length of the SPS,
which would relax certain parameters and ease an energy upgrade.}. 
%It is chosen also in order to limit the energy loss in the last return arc and as a result of
%a cost optimisation between the fractions of the circumference covered by SRF 
%and by return arcs.
 A 3-pass ERL  configuration had been adopted also for the FCC-eh
albeit maintaining the original $60$\,GeV as default which had a $9$\,km circumference. 

For the LHC, the ERL would be tangential to IP2. According to current plans, 
IP2 is given to the ALICE detector with a program extending to LS4,
 the first long shutdown following
the three year pause of the LHC operation for upgrading the luminosity performance
and detectors. There are plans for a new heavy ion detector to move into IP2. 
 The LS4 shutdown is currently scheduled to begin in 2031
with certain likelihood of being postponed to 2032 or later as recent events seem
to move LS3 forward and extend its duration to three years.


For FCC-eh
the preferred position is interaction point L, for geological reasons mainly, and the time of operation fully depending on the progress with FCC-hh, beginning at the earliest in the late 40ies if
CERN went for the hadron collider directly after the LHC.

The LHeC operation is transparent to the LHC collider experiments owing to
the low lepton bunch charge and resulting small beam-beam tune shift
experienced by the protons. The LHeC is thus designed to run simultaneously with
$pp$ (or $p$A or AA) collisions with a dedicated final operation of a few years.
% This tames  the cost and optimises the
%physics return since a concurrent operation with the LHC will have a direct
%impact on the HL-LHC physics programme as is sketched subsequently.

The paper presents in considerable detail the design of the LHeC (\textbf{Chapter\,10)}, i.e.\ the
optics and lattice, components, magnets, as well as
designs of the linac and interaction region besides special topics such 
as the prospects for electron-ion scattering, positron-proton operation and a novel
study of beam-beam interaction effects. With the more ambitious luminosity goal,
with a new lattice adapted to $50$\,GeV, with progress on the IR design,
a novel analysis of the civil engineering works and,
especially, the production and successful test~\cite{Marhauser:2018jxn} of the first  SC cavity 
at the newly chosen default frequency of $801.58$\,MHz, this report 
considerably extends beyond the initial CDR.  This holds especially since several
LHeC institutes have recently embarked on the development of the 
ERL technology with a low energy facility, PERLE, to be built at IJC Laboratory at Orsay. 


\subsection{PERLE}

Large progress has been made in the development
of superconducting, high gradient cavities with quality factors, $Q_0$, beyond $10^{10}$.
This will enable the exploitation of  ERLs in high-energy physics colliders, with the 
LHeC as a prime example, while considerations are also brought forward for future
$e^+e^-$ colliders~\cite{Litvinenko:2019txu} and for proton beam cooling with an ERL
tangential to eRHIC. 
The status and challenges of energy recovery linacs
are summarised in \textbf{Chapter~\,11}. This chapter
 also presents the design, status and prospects for
the ERL development facility PERLE. The major parameters of PERLE have been taken
from the LHeC, such as the 3-turn configuration, source,  frequency and 
cavity-cryomodule technology, in order to make 
PERLE a suitable facility for the development of LHeC ERL technology
and the accumulation of operating experience prior to and later in parallel with the LHeC.

An international collaboration has been established to build PERLE at Orsay. With the design goals
of $500$\,MeV electron energy, obtained in three passes through two cryo-modules and
of $20$\,mA, corresponding to $500$\,nC charge at $40$\,MHz bunch frequency, PERLE
is set to become the first ERL facility to operate at $10$\,MW power. Following its
CDR~\cite{Angal-Kalinin:2017iup} and a paper submitted to 
the European strategy~\cite{Klein:2652336}, work is directed 
to build a first dressed cavity and to release a TDR by 2021/22. Besides its value
for accelerator and ERL technology, PERLE is also of importance for pursuing
a low energy physics programme, see~\cite{Angal-Kalinin:2017iup}, and for
several possible industrial applications. It also serves as a local hub for the education
of accelerator physicists at a place, previously called Linear Accelerator Laboratory (LAL),
which has long been at the forefront of accelerator design and operation. 

There are a number of related ERL projects as are characterised in Chapter\,11.
The realisation of the ERL for the LHeC at CERN
represents a unique opportunity not only for physics and technology but as well
for a next and the current generation of accelerator physicists, engineers and technicians
to realise an ambitious collider project while the plans for very expensive next machines
may take shape. Similarly, this holds for a new generation of detector experts, as the
design of the upgrade of the general purpose detectors (GPDs) at the LHC is reaching completion, with the question
increasingly posed about opportunities for new collider detector construction to not loose
the expertise nor the infrastructure for building trackers, calorimeters and alike. The
LHeC offers the opportunity for a novel $4\pi$ particle physics detector design, construction
and operation. As a linac-ring collider, it may serve one detector of a size smaller than CMS and larger than H1 or ZEUS.
%

\subsection{The Detector}
%
\textbf{Chapter\,12} on the detector relies to a large extent on the very detailed write-up
on the kinematics, design considerations, and realisation of a detector for the LHeC
presented in the CDR~\cite{AbelleiraFernandez:2012cc}. In the previous report
one finds detailed studies not only on the central detector and its magnets, a central solenoid for
momentum measurements and an extended dipole for ensuring head-on $ep$ collisions,
 but as well on the forward ($p$ and $n$) and backward ($e$ and $\gamma$) tagging devices.
 The work on the detector as presented here was focussed on an optimisation of the
 performance and on the scaling of the design towards higher proton beam energies. It presents
 a new, consistent design and summaries of the essential characteristics in support of
 many physics analyses that this paper entails.
 
 The most demanding performance requirements arise from the $ep$ Higgs measurement programme,
 especially the large acceptance and high precision desirable for heavy flavour tagging and
 the requirement to resolve the hadronic final state. This has been influenced by both the rapidity
 acceptance extensions and the technology progress of the HL-LHC detector upgrades. A key example, also discussed, is the HV-CMOS Silicon technology, for which the LHeC 
 is an ideal application due to the much limited radiation level as compared to $pp$.
 
  Therefore we have now completed two studies of design: previously,
  of a rather conventional detector with limited cost and, here, of a more ambitious device. Both
  of these designs appear feasible. This regards also the installation. The paper
  presents a brief description of the installation of the LHeC detector at IP2 with the result
  that it may proceed within two years, including the dismantling of the 
  there residing detector. This
  calls for modularity and pre-mounting of detector elements on the surface, as was done
  for CMS too. It will be for the LHeC detector Collaboration, to be established with and for the
  approval of the project, to eventually design the detector according to its understanding and 
technical capabilities. 

\section{Outline}

The paper is organised as follows. For a brief overview, Chapter\,2 summarises the LHeC characteristics. 
Chapter\,3 presents the physics of the LHeC seen as a microscope for
measuring PDFs and exploring the 3D structure of the proton.
Chapter\,4 contains further means to explore QCD, especially low $x$ dynamics, together with two sections on QCD theory developments.
Chapter\,5 describes the electroweak and top physics potential of the LHeC.
Chapter\,6 presents the seminal nuclear particle physics
potential of the LHeC, through luminous electron-ion scattering exploring
an unexplored kinematic territory.
Chapter\,7 presents a detailed analysis of the opportunity for precision SM Higgs boson 
physics with charged and neutral current
  $ep$ scattering. Chapter\,8 is a description of the salient
opportunities to discover physics beyond the Standard Model with the LHeC, including non-SM
Higgs physics, right-handed neutrinos, physics of the dark sector, heavy resonances
and exotic substructure phenomena.
Chapter\,9 describes the interplay of $ep$ and $pp$ physics, i.e. the necessity to have
the LHeC for fully exploiting the potential of the LHC facility, e.g. through
the large increase of electroweak precision measurements, the considerable extension of
 search ranges and the joint $ep$ and $pp$ Higgs physics potential.
Chapter\,10 presents the update of the design on the electron accelerator with many novel results
such as on the lattice and interaction region, updated parameters for $ep$ and $eA$ scattering,
new specifications of components, updates on the electron source,$\dots$ The chapter
also presents the encouraging  results of the first LHeC $802$\,MHz cavity. Chapter\,11 is 
devoted, first, to the status and challenges of energy recovery based accelerators and, second,
to the description of the PERLE facility between its CDR and a forthcoming TDR. Chapter\,12 
describes the update of the detector studies towards an optimum configuration in terms
of acceptance and performance. Chapter\,13 presents a summary of the paper including a time line
for realising the LHeC to operate with the LHC. An Appendix 
presents the statement of the International Advisory Committee
on its evaluation of the project together with recommendations about how to proceed. It also contains an account for the membership
in the LHeC organisation, i.e. the Coordination Group and finally the
list of Physics Working Group convenors. 







%{\color{blue}SJB.} The collisions of $60$ GeV electrons with the $7$ TeV protons of the high energy LHC collider at CERN can provide a new testing ground for fundamental hadronic physics, as well as novel tests of the Standard Model. The tests include electroweak theory, precision QCD, lepto-quark phenomenology, and supersymmetric extensions of the Standard Model.
%A footnote from SJB\,\footnote{
%The  center-of-mas energy  at the proposed LHeC of $\sqrt{s}\simeq  1.3\,\TeV$ -- equivalent to an electron laboratory energy  
%$E^{lab} _e \simeq 1700\,\TeV$ -- would require a  17\,000 mile-long fixed-target accelerator based on the technology of the 50\,GeV two-mile SLAC linear accelerator.}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do not remove!
\biblio
\end{document}
