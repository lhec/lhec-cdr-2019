% !TEX encoding = UTF-8 Unicode

% modular compilation
\usepackage{subfiles}

%bibstyle
%\bibliographystyle{unsrt}
\bibliographystyle{\main/main/utphys}
\usepackage{cite}  % compactify multiple citatation, e.g. [1-3], rather [1][2][3]

% load all required packages

% general
\usepackage[l2tabu,orthodox]{nag}  % force newer (and safer) LaTeX commands
\usepackage{geometry}
 \geometry{
 a4paper,% total={170mm,257mm},
 total={160mm,237mm},
 left=25mm,
 top=25mm,
 }
\usepackage[utf8]{inputenc}        % set character set to UTF-8 (unicode)
\usepackage{babel}                 % multi-language support
%\usepackage{sectsty}               % allow redefinition of section command formatting
\usepackage{xcolor}                % more color options
%\usepackage{tocloft}               % redefine table of contents and define new like list
\usepackage{graphicx}
\usepackage{paralist}              % compactitem
\usepackage{listings}              % display shell commands and/or computer code
\usepackage{threeparttable}        % Used for tables in the accelerator part
\usepackage{ textcomp }            % permille symbol

% overall graphic path(s)
\graphicspath{{\main/main/figures/}{\main/main/figures/sjb/}{\main/main/}}

% misc
%\usepackage{todonotes} % add to do notes
\usepackage{float}     % floats
\usepackage{makecell}

% science
\usepackage{amsmath}                                    % extensive math options
\usepackage{amssymb}                                    % special math symbols
\usepackage{slashed}                                    % slashed symbols
\usepackage{xspace}                                     % spaces
%\usepackage[Gray,squaren,thinqspace,thinspace]{../sty/SIunits} % elegant units
%\usepackage{\main/main/SIunits}
%\usepackage{\main/sty/siunitx/siunitx}
\usepackage{siunitx}
\usepackage{booktabs}
\usepackage{tabularx,ragged2e}

%\usepackage{\main/sty/ltxutil}
%\usepackage{dcolumn}

% NEEDS to be before hyperref, hyperref
% number figures, tables and equations within the sections
%\numberwithin{equation}{section}
%\numberwithin{figure}{section}
%\numberwithin{table}{section}

% references and annotation
%\usepackage[small,bf,hang]{caption}        % captions
\usepackage[small,bf]{caption}        % captions
\usepackage{subcaption}                    % adds subfigure, subcaption
%\renewenvironment{table}[4]                % set default font size for tables
%     {\@float{table} \centering\small}     % font size, and centered
%     {\end@float}
\usepackage{hyperref}                      % add hyperlinks to references
\usepackage[noabbrev,nameinlink]{cleveref} % better references than default \ref
%\let\globcount\newcount
%\usepackage{autonum}                       % only number referenced equations (causes to include etex, but this causes problems)
\usepackage{url}                           % urls
\usepackage{lineno}                        % use line numbers
\usepackage{emptypage}                     % start chapters on odd pages


% format hyperlinks
\colorlet{linkcolour}{black}
\colorlet{urlcolour}{blue}
\hypersetup{colorlinks=true,linkcolor=linkcolour,citecolor=linkcolour,urlcolor=urlcolour}

\usepackage{\main/LHeC_and_HLLHC/atlasjetetmiss}
\usepackage{\main/LHeC_and_HLLHC/ANA-STDM-2018-22-PUB-defs}

\usepackage{lscape}      % PRN - for detector tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\setlength{\textwidth}{18cm}
%\setlength{\textheight}{24cm}
%\setlength{\oddsidemargin}{-1cm}
%\setlength{\topmargin}{-2.8cm}

%\setlength{\textwidth}{18cm}
%\setlength{\textheight}{24cm}
%\setlength{\oddsidemargin}{-1cm}
%\setlength{\topmargin}{-2.8cm}


% do not indent first line of paragraph!
%\setlength{\parindent}{0pt}                                                                                            
\usepackage{parskip}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LHeC macros...
\def\subfilestableofcontents{\tableofcontents}

\def\biblio{
%  \bibliographystyle{unsrt}
  \renewcommand{\bibname}{Bibliography $\normalfont{\textrm{[read~the~footnote]}}$\,\footnote[999]{ \normalsize {\textbf Note on bibliography}:
      \begin{compactitem}
      \item Put all \emph{bibitems} into file \textit{../lhec.bib}. 
      \item Use `inspirehep.net' bibtex entries whenever possible
      \item Check for duplicates in the bib file
      \item Use entry-type {\color{blue}'\texttt{@article}'} for regular articles (also \texttt{doi} and/or \texttt{arXiv} references are used). Use entry-type {\color{blue}\texttt{@inproceedings}} for other items (using at least: \texttt{author}. Specify then \texttt{Publisher} or \texttt{booktitle} where this article appeared (i.e.\ a talk, a chapter in a book, a technical note, a webpage, etc...), and provide \texttt{title}, \texttt{year}, \texttt{month}, \texttt{url}, \texttt{doi}, etc. if available.). 
      \item Do not forget to commit the updated .bib file to the repository
      \item There will be only a single bibliography at the end of the CDR document. This bibliography section is only for editing purposes and disappears in the final document.
  \end{compactitem}} }
  \bibliography{\main/lhec}
} 

\def\lhecinstructions{
  \input{../main/instructions.tex}
}
\def\lhectitlepage{
  \input{../main/titlepage.tex}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

