% !TEX encoding = UTF-8 Unicode
\providecommand{\main}{..} %Important: It needs to be defined before the documentclass
\documentclass[english,\main/main/lhec_2019.tex]{subfiles} % document type and language
% graphics path(s) for this section
\graphicspath{{\main/main/figures/}{\main/main/}{\main/characteristics/figures/}{\main/characteristics/}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do not edit!
\begin{document}
\linenumbers
\lhectitlepage
\lhecinstructions
\subfilestableofcontents


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapter starts here !
%\chapter{Main Characteristics of the LHeC \ourauthor{Oliver Bruening, Max Klein}}
%
%Example reference: LHeC~\cite{AbelleiraFernandez:2012cc}
%
%\begin{figure}[htbp]
%   \centering
%   \includegraphics[clip=,width=0.35\textwidth]{satkand}
%   \caption{Example figure, please remove}
%   \label{fig:examplefig}
%\end{figure}

%\section{Kinematics and Reconstruction of Final States \ourauthor{Max Klein}}
%
%\subsection{Nominal Beam Energies}
%
%\subsection{Reduced Electron or Proton Beam Energy}
%
\chapter{LHeC Configuration and Parameters}
\section{Introduction}
%
The Conceptual Design Report (CDR) of the LHeC was published in 2012~\cite{AbelleiraFernandez:2012cc}. 
The CDR default configuration uses a $60$\,GeV energy electron 
beam derived from
a racetrack, three-turn, intense energy recovery linac (ERL)  achieving 
a cms energy of $\sqrt{s}=1.3$\,TeV, where $s=4E_pE_e$ is determined 
by the electron and proton beam energies, $E_e$ and $E_p$. In 2012, 
the Higgs boson, $H$, was discovered which has become a central topic 
of current and future high energy physics. The Higgs production cross 
section in charged current (CC) deep inelastic scattering (DIS) at the 
LHeC is roughly $100$\,fb. The Large Hadron Collider has so far not led 
to the discovery of any exotic phenomenon. This forces searches to be 
pursued, in $pp$ but as well in $ep$, with the highest achievable precision
 in order to access a maximum range of phase space and possibly rare 
channels. The DIS cross section at large $x$ roughly behaves like
 $(1-x)^3/Q^4$, demanding very high luminosities for exploiting the 
unknown regions of Bjorken $x$ near  $1$ and very high $Q^2$, the 
negative four-momentum transfer squared between the electron and the 
proton. For the current update of the design of the LHeC this has set
 a luminosity goal about an order of magnitude higher
than the $10^{33}$\,cm$^{-2}$s$^{-1}$ which
had been adopted for the CDR. There arises the potential, as 
described subsequently in this paper, to transform the LHC into
a high precision electroweak, Higgs and top quark physics facility. 

The $ep$ Higgs production cross section rises approximately with $E_e$. 
New physics may be related to the heaviest known elementary particle, 
the top quark, the $ep$ production cross section of which rises more strongly 
than linearly with $E_e$ in the LHeC kinematic range as that is not very far 
from the $t \bar{t}$ threshold. Searches for heavy neutrinos, SUSY particles, 
etc.\ are the more promising the higher the energy is. 
The region of deep inelastic scattering and pQCD
requires that $Q^2$ be larger than $ M_p^2 \simeq 1$\,GeV$^2$.
Access with DIS to very low Bjorken $x$ requires high energies because of
$x=Q^2/s$, for inelasticity $y=1$. In DIS, one needs $Q^2 > M_p^2 \simeq 1$\,GeV$^2$.
 Physics therefore requires a maximally large energy. However, 
cost and effort set realistic limits such that twice the HERA 
electron beam energy, of about $27$\,GeV, 
appeared as a reasonable and affordable target value. 
 
In the CDR~\cite{AbelleiraFernandez:2012cc}
 the default electron energy was chosen to be $60$\,GeV. 
This can be achieved with an ERL circumference of $1/3$ of that of 
the LHC. Recently, the cost was estimated in quite some
 detail~\cite{ocost}, comparing also with other accelerator projects. Aiming 
at a cost optimisation and providing an option for a staged 
installation, the cost estimate lead to defining a new default configuration 
of $E_e=50$\,GeV with the option of starting in an initial phase 
with a beam energy of $E_e=30$\,GeV and a
a circumference  of $5.4$\,km which is $1/5$ of the LHC length.
Lowering $E_e$ is also advantageous for mastering the synchrotron radiation
challenges in the interaction region. Naturally, 
the decision on $E_e$ is not taken now. This paper comprises studies with 
different energy configurations, mainly $E_e=50$ and $60$\,GeV, which are close
in their centre-of-mass energy values of $1.2$ and $1.3$\,TeV, respectively.

Up to beam energies of about $60$\,GeV, the ERL cost is dominated by 
the cost for the superconducting RF of the linacs. Up to this energy 
the ERL cost scales approximately  linearly with the beam energy. Above
 this energy the return arcs represent the main contribution to the cost 
and to the ERL cost scaling is no longer linear.  Given the non-linear 
dependence of the cost on $E_e$, for energies larger than about $60$\,GeV, 
significantly larger electron beam energy values may only be justified 
by overriding arguments, such as, for example, the existence of 
leptoquarks~\footnote{If these existed with a mass of say $M=1.5$\,TeV 
this would require, at the LHC with $E_p=7$\,TeV, to choose $E_e$ to be
 larger than $90$\,GeV, and to pay for it. Leptoquarks would be produced
 by $ep$ fusion and appear as resonances, much like the $Z$ boson in
 $e^+e^-$ and would therefore fix $E_e$ (given certain $E_p$ which at 
the FCC exceeds $7$\,TeV). The genuine DIS kinematics, however, is 
spacelike, the exchanged four-momentum squared $q^2=-Q^2$ being negative,
 which implies that the choice of the energies is less constrained than 
in an $e^+e^-$ collider aiming at the study of the $Z$ or $H$ bosons.}.
Higher values of $\sqrt{s}$ are also provided with enlarged proton beam energies 
by the High Energy LHC ($E_p=13.5$\,TeV)~\cite{Abada:2019ono}
 and the FCC-hh~\cite{Benedikt:2018csr} with $E_p$ between 
$20$ and possibly $75$\,TeV, depending on the dipole magnet technology. 

%This paper presents a collection and update of parameters describing the LHeC configuration and its parameters. Section\,\ref{sec:cost} repeats tables from the cost estimate~\cite{ocost}. Section\,\ref{sec:scaled} summarises the parameters relevant to civil engineering. 
%Section\,\ref{sec:parlu} contains a summary of the parameters relevant to the luminosity including electron-ion scattering. Finally,
%Section\,\ref{sec:parlin} summarises the  main LINAC parameters.
%
\section{Cost Estimate, Default Configuration and Staging}
\label{subsec:cost}
%
In 2018 a detailed cost estimate was carried out~\cite{ocost} following 
the guidance and practice of CERN accelerator studies. The assumptions were 
also compared with the DESY XFEL cost. The result was that for the $60$\,GeV 
configuration about half of the total cost was due to the two SC linacs.  The
 cost of the arcs decreases more strongly than linearly with decreasing energy,
about $\propto E^4$ for synchrotron radiation losses and $\propto E^3$ when 
emittance dilution is required to be avoided~\cite{alexBerlin}.  It was 
therefore considered to set a new default of $50$\,GeV with a circumference 
of $1/5$ of that of the LHC, see Sect.\,\ref{subsec:config}, compared to 1/3 for
 $60$\,GeV. Furthermore, an initial phase at $30$\,GeV was considered, within 
the $1/5$ configuration but with only partially 
equipped linacs. The  HERA electron beam
 energy was $27$\,GeV. The main results, taken from~\cite{ocost} are 
reproduced in Tab.\,\ref{tab:cost}.
% 
\begin{table}[ht]
  \centering
  \small  
  \begin{tabular}{lccc}
    \toprule
    Component   & CDR 2012 & Stage 1 & Default  \\
                &  (60\,GeV)  &   (30\,GeV)  &  (50\,GeV)   \\
    \midrule
    SRF System   & 805 &   402  &  670 \\
    SRF R+D and Prototyping  &  31 & 31 & 31 \\
    Injector   &  40 & 40 &  40 \\
    Arc Magnets and Vacuum & 215 & 103 &  103   \\
    SC IR Magnets & 105  &  105  &   105  \\
    Source and Dump System & 5 & 5 &  5 \\
    Cryogenic Infrastructure & 100  & 41   &  69  \\
    General Infrastructure and Installation & 69 & 58  & 58  \\
    Civil Engineering &  386   & 289  &   289   \\
    \midrule
    Total Cost   & 1756  & 1075   &   1371   \\
    \bottomrule
  \end{tabular}
  \caption{Summary of cost estimates,
    in MCHF, from~\cite{ocost}. The $60$\,GeV configuration 
    is built with a $9$\,km triple racetrack configuration as
    was considered in the CDR~\cite{AbelleiraFernandez:2012cc}.
    It is taken as the default configuration for FCC-eh, with 
    an additional CE cost of $40$\,MCHF due to the 
    larger depth on point L (FCC) as compared to IP2 (LHC).
    Both the $30$ and the $50$\,GeV assume a $5.4$\,km configuration,
    i.e.\ the $30$\,GeV is assumed to be a first stage of LHeC upgradeable
    to  $50$\,GeV ERL. Whenever a choice was to be made on estimates, 
    in~\cite{ocost} the conservative number was chosen. 
    %The last column
    %represents a rough extrapolation to a non-upgradable 30 GeV configuration here only presented for
    %curiosity, see text.
  }
  \label{tab:cost}
\end{table}
%

The choice of a default of $50$\,GeV at 1/5 of the LHC circumference results, as displayed, in a total cost 
of $1,075$\,MCHF for the initial $30$\,GeV configuration and an additional,
 upgrade cost to $50$\,GeV of $296$\,MCHF. If one restricted the  
LHeC to a non-upgradeable $30$\,GeV only configuration one would,
 still in a triple racetrack configuration, come to roughly a $1$\,km
 long structure with two linacs of about $500$\,m length, probably in 
a single linac tunnel configuration. The cost of this version of the 
LHeC is roughly $800$\,MCHF, i.e.\ about half the $60$\,GeV estimated cost. However,
this would essentially reduce the LHeC to a QCD and electroweak machine, still
very powerful but accepting substantial losses in its Higgs, top and BSM programme. 

A detailed study was made on the cost of the civil engineering, which is also
discussed subsequently. This concerned a comparison of the 1/3 vs the 1/5 LHC
circumference versions, and the FCC-eh. The result is illustrated in 
Fig.\,\ref{fig:cecost}. It shows that the CE cost for the 1/5 version is about 
a quarter of the total cost. The reduction from 1/3 to 1/5 economises 
about $100$\,MCHF.
\begin{figure}[h]
\centering
\includegraphics*[width=0.7\textwidth]{cecost}
\caption{
Cost estimate for the civil engineering work for the tunnel, rf galleries and shafts
for the LHeC at 1/5 of the LHC circumference (left), at 1/3 (middle) and the
FCC-eh (right). The unit costs and percentages are consistent
 with FCC and CLIC unit prices. The estimate is considered reliable to $30$\,\%.
The cost estimates include: Site investigations: $2$\,\%, Preliminary design, tender
documents and project changes: $12$\,\% and the Contractors profit: $3$\,\%. Surface
site work is not included, which for LHeC exists with IP2.}
\label{fig:cecost}
\end{figure}

Choices of the final energy will be made later. They  depend not only  on a budget
 but also on the future development of particle physics at large. For example, it
 may turn out that, for some years into the 
 %% foreseeable %% this means we know what will happen 
 future, the community   may not find the $\mathcal{O}$(10)\,GCHF 
required to build any of the $e^+e^-$ colliders currently considered. Then the only way 
to improve on the Higgs measurements beyond HL-LHC substantially is the high energy 
($50 - 60$\,GeV), high luminosity ($\int{L} = 1$\,ab$^{-1}$) LHeC. 
 Obviously, physics and cost are intimately related. 
Based on such considerations, but also taking into account technical constraints as
resulting from  
the amount of synchrotron radiation losses in the interaction region
 and the arcs, we have chosen $50$\,GeV in a $1/5$ of U(LHC) configuration
 as the new default. This economises about $400$\,MCHF as
compared to the CDR configuration.

If the LHeC ERL were built, it may later be transferred, with some 
reconfiguration and upgrades, to the FCC to serve as the FCC-eh. 
The  FCC-eh has its own location, L, for the ERL which requires a 
new accelerator tunnel. It has been decided to 
keep the $60$\,GeV configuration for the FCC, as described in the 
recently published CDR of the FCC~\cite{Benedikt:2018csr}. The LHeC
ERL configuration may also be used as a top-up injector for the $Z$ 
and possibly $WW$ phase of the FCC-e should the FCC-ee indeed 
precede the FCC-hh/eh phase. 

%
\section{Configuration Parameters}
\label{subsec:config}
%
A possible transition from the $60$\,GeV to the $50$\,GeV configuration
 of the LHeC was already envisaged  in 2018, as considered in the  
paper submitted to the European Strategy~\cite{Bruning:2019scy}. The 
machine layout shown in that paper is  reproduced in Fig.\,\ref{fig:lhecconf}.
 It is a rough sketch illustrating the reduction from a $60$\,GeV to a $50$\,GeV 
configuration, which results not only in a reduction of capital costs, as discussed 
above, but also of effort.
% 
\begin{figure}[th]
%\centerline{\includegraphics*[width=100mm]{figlhec}}
\centering
\includegraphics[width=0.7\textwidth]{ring60}
\caption{
Schematic view of the three-turn LHeC configuration with two oppositely positioned 
electron linacs and three arcs housed in the same tunnel. Two configurations are 
shown: Outer: Default $E_e = 60$\,GeV with linacs of about $1$\,km length and $1$\,km arc radius
leading to an ERL circumference of about $9$\,km, or $1/3$ of the LHC length.
Inner: Sketch for $E_e = 50$\,GeV with linacs of about $0.8$\,km length and $0.55$\,km 
arc radius leading to an ERL circumference of $5.4$\,km, 
or $1/5$ of the LHC length, which is smaller than the size of the SPS.
The $1/5$ circumference configuration is flexible: it 
entails the possibility to stage the project as funds of physics dictate by using 
only partially equipped linacs, and it also permits upgrading to somewhat higher 
energies if one admits increased synchrotron power losses and operates at higher gradients.
}
\label{fig:lhecconf}
\end{figure}
%

The ERL configuration has been recently revisited~\cite{alexBerlin} considering
its dependence on the electron beam energy. Applying a dimension scaling which
preserves the emittance dilution, the  results have been obtained as are 
summarised in Tab.\,\ref{tab:scaledim}.
%
\begin{table}[ht]
   \centering
   \small
   \begin{tabular}{lccccc}
     \toprule
     Parameter & Unit & \multicolumn{4}{c}{LHeC option} \\
     %Item & LHeC & LHeC  & LHeC & LHeC \\
     \cmidrule{3-6}
     %     item   ~~~~~~~~~~~Circumference [U(LHC)] & (1/3 LHC)  & (1/4 LHC)  & (1/5 LHC) & (1/6 LHC)
      & & 1/3 LHC  & 1/4 LHC  & 1/5 LHC & 1/6 LHC \\
     \midrule
     Circumference & m & 9000 & 6750 &  5332 & 4500 \\
     Arc radius &  m\,$\cdot$\, $2\pi$ & 1058 & 737 & 536 &  427 \\
     Linac length &  m\,$\cdot$\,2 & 1025 & 909 &  829 & 758 \\
     Spreader and recombiner length &  m\,$\cdot$\,4 & 76 & 76 & 76 & 76 \\
     Electron energy &  GeV & 61.1 & 54.2 & 49.1 & 45.2 \\  
     \bottomrule
   \end{tabular}
 \caption{Scaling of the electron beam energy, linac and further 
   accelerator element dimensions with the 
   choice of the total circumference in units $1/n$ of the LHC circumference. For
   comparison, the CERN SPS has a circumference of 6.9\,km, only 
   somewhat larger than $1/4$ of that of the LHC.}
 \label{tab:scaledim}
\end{table}%
The $1/5$ configuration is chosen as the new LHeC default while the CDR on
 the LHeC from 2012 and the recent CDR on FCC-eh have used the 1/3 configuration. 
 The energy and configuration may  be decided as physics, cost and effort dictate,
 once a decision is taken.

%
\section{Luminosity}
\label{subsec:parlu}
%
The luminosity $L$ for the LHeC in its linac-ring configuration
is determined as
%
\begin{equation}
\label{LLR}
L = \frac{N_e N_p n_p f_{rev} \gamma_p}{4 \pi \epsilon_p \beta^*} \cdot \prod_{i=1}^3{H_i},
\end{equation}
where $N_{e(p)}$ is the number of electrons (protons) per bunch, $n_{p}$ the number of proton bunches in the LHC, 
$f_{rev}$ the revolution frequency in the LHC [the bunch spacing in a batch 
is given by $\Delta$, equal to $25$\,ns for protons in the LHC] and 
$\gamma_p$ the relativistic factor $E_p/M_p$ of the proton beam.
Further,  $\epsilon_p$ denotes the normalised proton transverse beam emittance 
and $\beta^*$  the proton beta function at the IP, assumed to be equal in 
$x$ and $y$. The luminosity is moderated by the hourglass factor, $H_1=H_{geo} \simeq 0.9$, the
pinch or beam-beam correction factor, $H_2=H_{b-b} \simeq 1.3$, and the 
filling factor $H_3=H_{coll} \simeq 0.8$, should an ion clearing gap in the
electron beam be required. This justifies taking the product of these factors. As the product is close to
unity, the factors are not listed for simplicity in the subsequent tables.

%and lead to a peak luminosity of $10^{33}$\,cm$^{-2}$s$^{-1}$.
The electron beam current is given as
\begin{equation}
  \label{Ie}
  I_e= e N_e  f\,,
\end{equation}
%
%where $I_e$ is given in mA, $P$ is the electron beam power, in MW,
%and $E_e$ the electron beam energy in GeV. 
%
where $f$ is the bunch frequency $1/\Delta$. The current for the LHeC is limited by the charge delivery of the source. In the new default design
we have $I_e=20$\,mA which results from a charge of $500$\,pC for the bunch frequency
of $40$\,MHz. It is one of the tasks of the PERLE facility to investigate the stability of the
3-turn ERL configuration in view of the challenge for each cavity to hold the sixfold current due to the
simultaneous acceleration and deceleration of bunches at three different beam energies each.

\subsection{Electron-Proton Collisions}
%
The design parameters of the luminosity were recently provided in a note describing the 
FCC-eh configuration~\cite{LHeClumi}, including the LHeC.  
Tab.\,\ref{tab:lumip} represents an update comprising in addition the initial
 $30$\,GeV configuration and the lower energy version of the FCC-hh  based on the LHC 
magnets\footnote{
The low energy FCC-pp collider, as of today, uses a 6\,T LHC 
magnet in a 100\,km tunnel. If, sometime in the coming decades, high field magnets  become available 
based on HTS technology, then a $20$\,TeV proton beam energy may even be
achievable in the LHC tunnel. To this extent the  low energy FCC  considered here and 
an HTS based HE-LHC would be comparable options in terms of their energy reach.
}.  
For the LHeC, as noted above, we assume $E_e=50$\,GeV while for FCC-eh we retain 
$60$\,GeV. Since the source limits the electron current, the peak luminosity
 may be taken not to depend on $E_e$.
 Studies of the interaction region design, presented in this paper, show that one may be confident of reaching
a $\beta^*$ of $10$\,cm but it will be a challenge to reach even smaller values. Similarly,
it will be quite a challenge to operate with a current much beyond $20$\,mA.
That has nevertheless been considered~\cite{Bordry:2018gri} 
for a possible dedicated LHeC operation mode for a few years following the $pp$ operation program. 
%Note that this is not before in twenty years which may well be expected
% to bring progress in technology such as the cavity quality and source intensity.
%
\begin{table}[ht]
  \centering
  \small
  \begin{tabular}{lcccccccc}
    %\hline
    %item & & LHeC & & FCC-eh & \\
    \toprule
    Parameter & Unit & \multicolumn{4}{c}{LHeC} & & \multicolumn{2}{c}{FCC-eh}  \\
    \cmidrule{3-6} \cmidrule{8-9}
     & & CDR  & Run 5 & Run 6 & Dedicated & & $E_p$=$20$\,\TeV & $E_p$=$50$\,\TeV \\
    \midrule
%    $E_p$ [TeV] & 7 & 7 & 7 & 7 & & 20 & 50 \\
    $E_e$ & GeV & 60 & 30  & 50  &  50 & & 60  &  60 \\
    $N_p$ & $10^{11}$ & 1.7 & 2.2  & 2.2 & 2.2  && 1 & 1 \\
    $\epsilon_p$ & $\mu$m & 3.7 & 2.5 & 2.5 & 2.5  && 2.2 & 2.2 \\
    $I_e$ & mA & 6.4 & 15  & 20 & 50  && 20 & 20 \\
    $N_e$ & $10^9$ & 1 & 2.3  & 3.1 & 7.8  && 3.1 & 3.1 \\
    $\beta^*$ & cm & 10 & 10 & 7 & 7  && 12 & 15 \\
%    \addlinespace
    Luminosity & $10^{33}$\,cm$^{-2}$s$^{-1}$ & 1 & 5 & 9 & 23& & 8 & 15 \\ 
    \bottomrule
  \end{tabular}
  \caption{Summary of luminosity parameter values for the LHeC and FCC-eh. Left: CDR 
    from 2012; Middle: LHeC in three stages, an initial run, possibly during Run\,5 of the LHC,
 the $50$\,GeV operation during Run\,6, both concurrently with the LHC, and a final,
 dedicated, stand-alone $ep$ phase; Right: FCC-eh with a 20
 and a 50\,TeV proton beam, in synchronous operation.}
  \label{tab:lumip}
\end{table}
%

The peak luminosity values exceed those at HERA by 2--3 orders of magnitude. 
The operation of HERA in its first, extended running period, 1992-2000, provided 
an integrated luminosity of about $0.1$\,fb$^{-1}$ for
the collider experiments H1 and ZEUS. This may now be expected to be taken in a day of initial LHeC operation. 
%
\subsection{Electron-Ion Collisions}

The design parameters and luminosity were also provided recently~\cite{LHeClumi}
for collisions of  electrons and lead nuclei (fully stripped 
$^{208}\mathrm{Pb}^{82+}$ ions). 
Tab.\,\ref{tab:lumeA} is an 
 update of the numbers presented there for consistency with the Run~6  
LHeC  configuration in Tab.~\ref{tab:lumip} and with the addition of 
 parameters corresponding to the 
$E_p=20\,\mathrm{TeV}$  FCC-hh configuration.  
Further discussion of this operating mode and motivations for the parameter choices 
in this table are provided in 
Section~\ref{sec:eAoperation}. 
 
\begin{table*}[ht] 
  \centering
  \small
  \begin{tabular}{lcccc}
    \toprule
    Parameter  & Unit  & LHeC  & FCC-eh & FCC-eh \\
               &       &       & ($E_p$=20\,\TeV) & ($E_p$=50\,\TeV) \\
    \midrule
  %%  Equivalent proton energy $E_p$  &  TeV  & 7  & 20 & 50 \\
    Ion  energy $E_\mathrm{Pb} $ & PeV &   0.574 &  1.64 &  4.1 \\ 
    Ion energy/nucleon $E_\mathrm{Pb}/A $ & TeV &   2.76 &  7.88 &  19.7 \\ 
    Electron beam energy $E_e$ & GeV  & 50 & 60 & 60 \\
    Electron-nucleon CMS  $\sqrt{s_{eN}}$  & TeV  & 0.74 & 1.4 & 2.2 \\
    Bunch spacing & ns  & 50 & 100 & 100 \\
    Number of bunches & & 1200 & 2072 & 2072 \\
    Ions per bunch & $10^{8}$  & 1.8 & 1.8 & 1.8 \\
    Normalised emittance $\epsilon_n$ & $\mu$m  & 1.5 & 1.5 & 1.5 \\
    Electrons per bunch & $10^9$ & 6.2 & 6.2 & 6.2 \\
    Electron current & mA  & 20 & 20 & 20 \\
    IP beta function $\beta^*_A$ & cm  & 10 & 10 & 15 \\
    %hourglass factor~~H$_{geom}$  & 0.9 & 0.9& 0.9 \\
    %pinch factor~~H$_{b-b}$  & 1.3 & 1.3 & 1.3 \\
    %bunch filling~~H$_{coll}$  & 0.8  & 0.8 & 0.8 \\
    e-N Luminosity   & $10^{32}$cm$^{-2}$s$^{-1}$  & 7 & 14 & 35 \\
    \bottomrule
  \end{tabular}
  \caption{
    Baseline parameters of future electron-ion collider
    configurations based on the electron ERL, in concurrent
    $eA$ and $AA$ operation mode with the LHC and the two versions of a future hadron 
    collider at CERN. 
    Following established convention in this field, the luminosity quoted, at the start of a fill, is the 
    \emph{electron-nucleon} luminosity which is a factor $A$ larger than the usual (i.e.\ electron-nucleus) luminosity.
    }
  \label{tab:lumeA}
\end{table*} 

One can expect the average luminosity during fills to be 
about 50\% of the peak in Tab.\,\ref{tab:lumeA} and we assume 
an overall operational efficiency of 50\%.   Then, a year of 
$eA$ operation, possibly composed 
by combining shorter periods of operation,
would have the potential to provide an integrated data set of about $5~(25)$\,fb$^{-1}$ for
the LHeC (FCC-eh), respectively. 
This exceeds the HERA electron-proton luminosity value by about tenfold for the LHeC and much more at FCC-eh
while the fixed target nuclear DIS experiment kinematics is extended by 3--4 orders of magnitude.
 These energy frontier  electron-ion configurations therefore have the unique
potential to radically modify our present view of nuclear structure and parton dynamics. 
This is discussed in Chapter~4.

\section{Linac Parameters}
\label{subsec:parlin}
%
The brief summary of the main LHeC characteristics here concludes with
a table of the main ERL parameters for the new default electron energy of $50$\,GeV,
Tab.\,\ref{tab:linpar}, which are discussed in detail in Chapter\,8.
%
\begin{table}[ht]
  \centering
  \small
  \begin{tabular}{lcc}
    \toprule
    Parameter  & Unit & Value \\
    \midrule
    %
    Frequency  & MHz & 801.58  \\
    Bunch charge & pC & 499 \\
    Bunch spacing & ns & 24.95 \\ 
    Electron current & mA  & 20 \\
    Injector energy & MeV & 500 \\
    Gradient & MV/m & 19.73 \\
    Cavity length, active & m & 0.918 \\
    Cavity length, flange-to-flange & m & 1.5 \\
    Cavities per cryomodule &  &  4 \\
    Length of cryomodule & m & 7  \\
    Acceleration per cryomodule & MeV & 72.45  \\
    Total number of cryomodules &  & 112  \\
    %Transverse emittance & gamma mm mrad & 50 \\
    Acceleration energy per pass & GeV & 8.1 \\
    \bottomrule
  \end{tabular}
  \caption{Basic LHeC ERL characteristics for the default configuration
    using two such linacs located opposite to each other in a racetrack of
    $5.4$\,km length. Each linac is passed three times for acceleration and three
    times for deceleration.
  }
  \label{tab:linpar}
\end{table}%
%
%
%
\section{Operation Schedule}
%\renewcommand{\b}{\ensuremath{\beta}}
The LHeC parameters are determined to be compatible with a parasitic operation with the 
nominal HL-LHC proton-proton operation. This implies limiting the electron bunch current
to sufficiently small values so that the proton beam-beam parameter remains small enough
to be negligible for the proton beam dynamics.

Assuming a ten year construction period for the LHeC after approval of the project
 and a required installation window of two years for the LHeC detector, the earliest 
realistic operation period for the LHeC coincides with the LHC Run\,5 period in 2032 and 
with a detector installation during LS4 which is currently scheduled
during 2030 and would need to be extended by one year to 2031. The baseline HL-LHC operation
 mode assumes 160 days of proton operation, 20 days of ion operation and 20 days of machine
 development time for the Run 4 period, amounting to a total of 200 operation days per year. 
 After the Run 4 period the HL-LHC does not consider ion operation at present  and assumes 190 days for proton operation.
The HL-LHC project 
assumes an overall machine efficiency of $54$\,\% (e.g.\ fraction of scheduled operation time 
spent in physics production) and we assume that the ERL does not contribute to significant
 additional downtime for the operation. Assuming an initial $15$\,mA of electron beam current, a
 $\beta^{*}$ of $10$\,cm and HL-LHC proton beam parameters, the LHeC reaches a peak luminosity 
of $0.5\cdot 10^{34}$\,cm$^{-2}$s$^{-1}$. Assuming further a proton beam lifetime of 16.7 hours,
 a proton fill length of 11.7 hours and an average proton beam turnaround time of 4 hours,
 the LHeC can reach in this configuration an annual integrated luminosity of $20$\,fb$^{-1}$.

For the evaluation of the physics potential it is important to note that the Run\,5
initial $ep$ operation period may accumulate about $50$\,fb$^{-1}$ of integrated luminosity.
This is the hundredfold value which H1 (or ZEUS) took over a HERA lifetime of $15$ years.
As one may expect, for details see Chapter\,3, such a huge DIS luminosity is ample
for pursuing basically the complete QCD programme. In particular, the LHeC would deliver
on time for the HL-LHC precision analyses the external, precise PDFs and with just
a fraction of the $50$\,fb$^{-1}$ the secrets of low $x$ parton dynamics would unfold.
Higher $ep$ luminosity is necessary for ultimate precision and for
the top, BSM and the Higgs programme of the LHeC to be of competitive value.

For the Run\,6 period of the HL-LHC, the last of the HL-LHC operation periods, we assume
 that the number of machine development sessions for the LHC can be suppressed, providing
 an increase in the  operation time for physics production from 190 days to 200 days
 per year. Furthermore, we assume that the electron beam parameters can be slightly further 
pushed.
%, implying larger impact of the electron-proton beam collisions on the proton beam 
%dynamics [e.g. a larger transverse emittance blow-up and / or reduced proton beam lifetimes].
 Assuming a $\beta^{*}$ reduced to $7$\,cm, an electron beam current of up to $25$\,mA and still nominal 
HL-LHC proton beam parameters, the LHeC reaches a peak performance of $1.2 \cdot 10^{34}$\,cm$^{-2}$s$^{-1}$
 and an annual integrated luminosity of $50$\,fb$^{-1}$.
This would add up to an integrated luminosity of a few hundred fb$^{-1}$, 
a strong base for top, BSM and Higgs physics at the LHeC.

Beyond the HL-LHC exploitation period, the electron beam parameters
could be further pushed in dedicated $ep$ operation, when the requirement of a parasitic
 operation to the HL-LHC proton-proton operation may no longer be imposed. 
The proton beam lifetime without proton-proton collisions would be significantly 
larger than in the HL-LHC configuration. In the following we assume a proton beam
 lifetime of $100$ hours and a proton beam efficiency of $60$\,\% without proton-proton
 beam collisions. The electron beam current in this configuration would only be
 limited by the electron beam dynamics and the SRF beam current limit. Assuming
 that electron beam currents of up to $50$\,mA, the LHeC would reach a peak luminosity
 of $2.4\cdot 10^{34}$\,cm$^{-2}$s$^{-1}$ and an annual integrated luminosity of up to
 $180$\,fb$^{-1}$. Table\,\ref{op-schedule} summarises the LHeC configurations over these three 
periods of operation. 


%Fig.\,\ref{intlumi} illustrates the development of the luminosity
%increase with time.

\begin{table}[ht]
  \centering
  \small
  \begin{tabular}{lcccc}
    \toprule
    Parameter  & Unit & Run\,5 Period  & Run\,6 Period & Dedicated\\
    \midrule
    %
    Brightness $N_p/(\gamma\epsilon_p)$ & $10^{17}\text{m}^{-1}$ & 2.2/2.5 & 2.2/2.5 & 2.2/2.5  \\
    Electron beam current & mA & 15 & 25 & 50? \\
    Proton $\beta^*$ & m & 0.1 & 0.7 & 0.7 \\ 
    Peak luminosity & $10^{34}$\,\si{cm^{-2}s^{-1}}  & 0.5 & 1.2 & 2.4 \\
    Proton beam lifetime & h & 16.7 & 16.7 & 100 \\
    Fill duration & h & 11.7 & 11.7 & 21 \\
    Turnaround time & h & 4 & 4 & 3 \\
    Overall efficiency & \% & 54 & 54 & 60 \\
    Physics time / year & days &  160 & 180 & 185 \\
    Annual integrated lumi. & fb$^{-1}$ & 20 & 50 & 180  \\
    \bottomrule
  \end{tabular}
\caption{The LHeC performance levels during different operation modes.
}
\label{op-schedule}
\end{table}
%
%
Depending on the years available for a dedicated final operation (or through an
extension of the $pp$ LHC run, currently not planned but interesting for
collecting $4$ instead of $3$\,ab$^{-1}$ to, for example, observe di-Higgs production
at the LHC), a total luminosity of $1$\,ab$^{-1}$ could be available for the LHeC.
This would double the precision of Higgs couplings measured in $ep$ as compared to the
default HL-LHC run period with $ep$ added as described. It would also significantly
enlarge the potential to observe or/and quantify rare and new physics phenomena. Obviously
such considerations are subject to the grand developments at CERN. A period with most interesting
physics and on-site operation activity could be particularly welcome for narrowing a possible large time
gap between the LHC and its grand successor, the FCC-hh. One may, however, be interested
in ending LHC on time. It thus is important for the LHeC project to recognise its particular
value as an asset of the HL-LHC, and on its own, with even less than the ultimate luminosity,
albeit values which had been dreamt of at HERA.

% (re)moved
%\section{Summary of Acceptance, Resolution and Calibrations \ourauthor{Peter Kostka}}
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do not remove!
\biblio
\end{document}

