#!/bin/bash

echo ""
echo " *** NOTE *** "
echo ""
echo " Run pdflatex and bibtex in main first!"
echo " you need an updated .bbl file."
echo " "

/bin/cp main/lhec_2019.bbl lhec_2020.bbl
#tar czvf figures.tar.gz fcc/figures erl/figures detector/figures nuclearphysics/figures introduction/figures accelerator/figures characteristics/figures LHeC_and_HLLHC/figures standardmodel/figures higgs/figures conclusion/figures bsm/figures main/figures

tar czvf figures.tar.gz ./*/figures
tar czvf main.tar.gz --exclude='*/figures' --exclude='*/lhec_2019.pdf'  --exclude='*/makefile' fcc erl detector nuclearphysics introduction accelerator characteristics LHeC_and_HLLHC standardmodel higgs conclusion bsm main lhec_2020.bbl lhec_2020.tex 00README.XXX
#lhec.bib

