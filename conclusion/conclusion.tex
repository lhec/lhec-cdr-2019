
\providecommand{\main}{..} %Important: It needs to be defined before the documentclass
\documentclass[english,\main/main/lhec_2019.tex]{subfiles} % document type and language
% graphics path(s) for this section
\graphicspath{{\main/main/figures/}{\main/main/}{\main/conclusion/}{\main/conclusion/figures/}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do not edit!
\begin{document}
\linenumbers
\lhectitlepage
\lhecinstructions
\subfilestableofcontents


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapter starts here !
\chapter{Conclusion}
% \ourauthor{ Oliver Bruening, Max Klein}}
%\section{Summary \ourauthor{ Max Klein}}
%\section{Timeline and Future Project Development \ourauthor{ Oliver Bruening}}
%
The Large Hadron Collider determines the energy frontier of experimental
collider physics for the next two decades.  Following the current luminosity
upgrade, the LHC can be further upgraded with a high energy,
intense electron beam such that it becomes a twin-collider facility, in which
 $ep$ collisions are registered  concurrently with $pp$. 
 A joint ECFA, CERN and NuPECC initiative led to a 
detailed conceptual design report (CDR)~\cite{AbelleiraFernandez:2012cc} for the 
 Large Hadron Electron Collider published in 2012.
The present paper represents an update of the original CDR in view
of new physics and technology developments.

The LHeC uses a novel, energy recovery linear electron accelerator  which
enables TeV energy  electron-proton collisions at high luminosity,  
of $\mathcal{O}(10^{34})$\,cm$^{-2}$s$^{-1}$, 
exceeding that of HERA by nearly three orders of magnitude.   
The discovery  of the Higgs boson and the surprising absence of BSM physics 
at the LHC demand to  extend the experimental base of particle physics suitable
to explore the energy frontier, beyond $pp$ collisions at the LHC.
The LHC infrastructure is the largest single investment the European and
global particle physics community ever assembled, and the addition 
of an electron accelerator a seminal opportunity way to build on it, and
to sustain the HL-LHC programme by adding necessary elements
which are provided by high energy deep inelastic scattering. As has been
shown in this paper, the external DIS input transforms the LHC to a much 
more powerful facility, with a new level of resolving matter substructure,
a more precise Higgs programme, challenging and complementing 
that of a next $e^+e^-$ collider, and with a hugely extended potential
to discover physics beyond the Standard Model.

The very high luminosity and the substantial extension of the kinematic range
in deep inelastic scattering, compared to HERA, make the LHeC on its own
a uniquely powerful TeV energy  collider.
Realising the  $\it{Electrons~for~LHC}$ programme developed with the 
previous and the present ``white" papers, will create the
cleanest, high resolution microscope accessible to the world, which one may term the
 ``CERN Hubble Telescope for the Micro-Universe". 
 It is directed to unravel
the substructure of matter encoded in 
the complex dynamics of the strong interaction, and to provide the necessary input 
for  precision and discovery physics at 
the HL-LHC and for future hadron colliders. 

This programme, as has been described
in this paper, comprises the 
complete resolution of the partonic densities in an unexplored range of
kinematics, the foundations for new, generalised views on proton structure
and the long awaited clarification of the QCD dynamics at high densities, as
are observed at small Bjorken $x$. New high precision measurements on
diffraction and vector mesons will shed new light on the puzzle of confinement.
As a complement to the LHC and a possible future e$^+$e$^-$ machine, 
the LHeC would scrutinise the Standard Model deeper than 
ever before, and possibly discover new physics in the electroweak 
and chromodynamic sectors as is outlined in the paper. 

Through the extension of the kinematic range 
by about three orders of magnitude in lepton-nucleus ($e$A) scattering, 
the LHeC is the most powerful electron-ion research facility one can 
build in the next decades, 
for clarifying the partonic substructure 
and dynamics inside nuclei for the first time and elucidating the chromodynamic origin
of the Quark-Gluon-Plasma. 

The Higgs programme at the LHeC  is
 astonishing in its precision. It relies on CC and NC precision measurements for
which an inverse atobarn of luminosity is desirable to achieve.  The prospective 
results on the Higgs couplings
from the HL-LHC, when combined with those here presented from the LHeC, will determine
the couplings in the most frequent six Higgs decay channels to one per cent
level accuracy. This is as precise as one expects measurements from linear $e^+e^-$
colliders but obtained dominantly from $gg$ and $WW$ fusion respectively, as compared
to Higgs-strahlung in electron-positron scattering which has the advantage of 
providing a Higgs width determination too.  The combined $pp$+$ep$ LHC facility 
at CERN may then be expected to remain the centre of Higgs physics for two more decades.

Searches for BSM physics at the LHeC offer great complementarity to similar searches at the HL-LHC.
The core advantage of the LHeC is the clean, QCD-background and pileup-free environment of an
electron-proton collider with a cms energy exceeding a TeV.
% combined with the larger center-of-mass energies from the proton beam.
This enables discoveries of signatures that could be lost in the hadronic noise at $pp$ 
or possibly unaccessible due to the limited com energy at $ee$.
%A well-known example of BSM physics with great 
%prospects at the LHeC are leptoquarks that interact with the
%first generation of the SM fermions, which could be produced resonantly if sufficiently light, or show up
%indirectly via their induced contact interactions.
Prominent examples for discovery enabled with $ep$
%, where the LHeC significantly improves the prospects of discovery, 
are heavy neutral leptons (or sterile neutrinos) that mix with the electron flavour, 
dark photons below the di-muon threshold,
which are notoriously difficult to detect in other experiments,
 long-lived new particles in general or new
physics scenarios with a compressed mass spectrum, such as SUSY electrowikinos and heavy scalar resonances with masses around and below 500\,GeV, which may exist but  literally be buried in di-top backgrounds at the LHC.

%Further examples for new physics here studied include anomalous gauge boson and top quark couplings, which connect
%BSM physics to new studies of the SM.
%The complementarity of BSM searches between the LHeC and the HL-LHC makes plain the opportunities for BSM
%discoveries.

The LHeC physics programme reaches far beyond any  specialised 
goal which underlines the unique opportunity for particle physics
to build a novel laboratory for accelerator based energy frontier
research at CERN. The project is fundable within the CERN budget, 
and not preventing much  more massive investments into the further future. 
It offers the possibility for the current generation of accelerator
physicists to build a new collider using and developing novel
technology while preparations proceed for the next grand step in particle
physics for generations ahead.

The main technical innovation through the LHeC
is the first ever high energy application of energy recovery technology, based on
high quality superconducting RF developments, a major contribution
to the development of $green$ collider technology which is an appropriate
 response to demands of our time. The ERL technique is more and more seen to
have major further applications, beyond ep at HE-LHC and FCC-eh, such
 as  for FCC-ee, as a $\gamma \gamma$ Higgs facility or, 
 beyond particle physics, as the highest energy XFEL
of hugely increased brightness.

The paper describes the plans and configuration of PERLE, the
first $10$\,MW power ERL facility, which is being prepared 
in international collaboration for built
at Ir\`ene Joliot-Curie Laboratory at Orsay. PERLE has adopted the
3-pass configuration, cavity and cryomodule technology, source 
and injector layout, frequency and electron current parameters
from the LHeC. This qualifies it to be the ideal machine to 
accompany the development of the LHeC. However, through 
its technology innovation  and its challenging 
parameters, such as an intensity exceeding that of ELI by orders
of magnitude, PERLE has an independent, far reaching low energy
nuclear and particle physics programme with new and particularly 
precise measurements. It also has a possible program on industrial
applications, which has not been discussed in the present paper. 


The LHeC provides an opportunity for building a novel
collider detector which is sought for as the design of the HL-LHC
detector upgrades is approaching completion.
A novel $ep$ experiment enables modern detection technology, such as 
HV CMOS Silicon tracking, to be further developed
and exploited in a new generation, $4\pi$ acceptance, no pile-up, 
high precision collider detector in the decade(s) hence. This
paper presented an update of the 2012 detector design, 
in response to developments of physics,
 especially Higgs and BSM, and of technology in detectors and 
analysis. The LHeC requires to be installed at IP2 at the LHC for there is no other
interaction region available while the heavy ion programme at the LHC is
presently limited to the time until LS4. In the coming years it  will have to be
decided whether this or alternative proposals for using IP2 during the 
final years of LHC operation are considered attractive enough and realistic
to be realised.

The next steps in this development are rather clear: the emphasis on ERL, beyond LHeC,
requires the PERLE development to rapidly
proceed. Limited funds are to be found  for essential components with the
challenging IR quadrupole as the main example.  ECFA is about to establish a
detector and physics series of workshops, including possible future Higgs facilities, and $ep$, 
which is a stimulus for further developing the organisational base of the LHeC towards
a detector proto-Collaboration. These developments shall include preparations for FCC-he
and provide a necessary basis when in a few years time, as recommended by the
IAC, a decision on building the LHeC at CERN may be taken. 
%This will proceed in the context  of what these years may bring
%for physics, with higher LHC luminosity, and especially in Asia, with decisions
%about ILC and CepC.  For the particle physics community,
% the further future and support of CERN
%as the world's leading laboratory for particle physics are at stake, including 
%its way of cooperation globally and with its surrounding major laboratories.

The recent history teaches a lesson about the complementarity required
for energy frontier particle physics. In the seventies and eighties, 
CERN hosted the $p \bar{p}$ energy frontier, with UA1 and UA2,
and the most powerful DIS experiments with muons (EMC, BCDMS, NMC)
and neutrinos (CDHSW, CHARM), while $e^+e^-$ physics was pursued
at PEP, PETRA and also TRISTAN. Following this, the Fermi scale could be
explored with the Tevatron, HERA and LEP. The here advertised
next logical step is to complement the HL-LHC by a most powerful DIS facility,
the LHeC, while preparations will take shape for  a next $e^+e^-$ 
%and hadron-hadron
collider, currently at CERN and in Asia.  Hardly a decision on LHeC may be taken
independently of how the grand future unfolds. Still, this scenario would give a realistic
and yet exciting base for completing the exploration of TeV
scale physics which may not be achieved with solely the LHC. 

The ERL concept and technology here presented has the
potential to accompany the FCC for realising the FCC-eh machine
when the time comes for the next, higher energy hadron collider, and
the search for new physics at the $\mathcal{O}(10)$\,TeV scale.
% The current
%situation of particle physics reminds us on the potential we have
%when resources and prospects are unified, for which this study
%is considered to be a hopefully valuable contribution.

\vspace{1cm}
\begin{Large}
$\bf{Acknowledgement}$ \\
\end{Large}
\vspace{0.4cm}

\noindent
The analyses and developments here presented would not have been possible
without the CERN Directorate and other laboratories, universities
and groups supporting this
study. We admire the skills of the technicians who successfully built
the first $802$\,MHz SC cavity. We 
thank many of our colleagues for their interest in this work and a supportive
attitude when time constraints could have caused  lesser understanding.
 Special thanks are also due to the members and Chair of the International
Advisory Committee for their attention and guidance to the project.
From the beginning of the LHeC study, it has been supported by ECFA
and its chairs, which was a great help and stimulus for undertaking this
study performed outside our usual duties. During the time,
a number of students, in Master and PhD courses, have made very essential
contributions to this project for which we are especially grateful. This
also extends to colleagues with whom we have been working closely
but who meanwhile left this development, perhaps temporarily,
or work at non-particle physics institutions while wishing LHeC success. The current
situation of particle physics reminds us on the potential we have
when resources and prospects are combined, for which this study
is considered to be a contribution.

The authors would like to thank the Institute of Particle Physics Phenomenology (IPPP) at Durham
for the award of an IPPP Associateship to support this work, and gratefully acknowledges support
from the state of Baden-W\"urttemberg through bwHPC and the German Research Foundation (DFG) through grant no INST 39/963-1 FUGG.
Financial support by Ministerio de Ciencia e Innovaci\'on
of Spain under projects FPA2017-83814-P and Unidad de
Excelencia Mar\'{\i}a de Maetzu under project MDM-2016-0692, and
by Xunta de Galicia (project ED431C 2017/07 and Centro singular de investigación de Galicia accreditation 2019-2022) and the European Union (European Regional Development Fund – ERDF), is gratefully acknowledged.
It has been performed in the framework of COST Action CA
15213 “Theory of hot matter and relativistic heavy-ion
collisions” (THOR), MSCA RISE 823947 “Heavy ion collisions: collectivity and precision in saturation physics” (HIEIC) and has received funding from the European Union’s Horizon 2020 research and innovation programme
under grant agreement No. 824093.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do not remove!
\biblio
\end{document}

